from django.shortcuts import render


def test(request):
    return render(request, 'landing/test.html', )


def testm(request):
    return render(request, 'landing/test_mini.html', )


def landing(request):
    return render(request, 'landing/index.html', )


def blog(request):
    return render(request, 'landing/blog.html', )
