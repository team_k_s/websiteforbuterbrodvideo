# from django.conf.urls import url, include
from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.landing, name='landing'),
    path('index', views.landing, name='landing'),
    path('test', views.test, name='test'),
    path('blog', views.blog, name='blog'),
    path('testm', views.testm, name='testm'),
]
