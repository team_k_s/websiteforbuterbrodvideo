from django.shortcuts import render


def blog_include(request):
    return render(request, 'blog/index.html', )


def index(request):
    return render(request, 'blog/index.html', )
