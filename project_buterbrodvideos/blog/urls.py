# from django.conf.urls import url, include
from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.blog_include, name='include'),
    path('index', views.index, name='index'),
]
